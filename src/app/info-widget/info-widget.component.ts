import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { MetricsService } from '../metrics.service';


@Component({
  selector: 'info-widget',
  templateUrl: './info-widget.component.html',
  styleUrls: ['./info-widget.component.css']
})
export class InfoWidgetComponent implements OnInit, OnChanges {

  @Input() infoType: string;
  @Input() selectedPeriod: string;
  @Input() selectedCategory: string;
	@Input() selectedManufacturer: string;
	iconPath: string;
	value: number;
	variation: number;
	title: string;
	text: string;
	obs: string;
	valuePercentage: boolean;
	variationPercentage: boolean;
	seconds: boolean;

  constructor(private metricsService: MetricsService) { }

  setWidget(iconPath: string, title: string, text: string, obs: string): void {
    this.iconPath = iconPath;
    this.title = title;
    this.text = text;
    this.obs = obs;
  }

  setStoreFlow(): void {
    this.metricsService.getMetrics().subscribe((data) => {
      this.value = data[this.selectedPeriod].metrics.store_exposure;
      this.variation = data[this.selectedPeriod].metrics.store_exposure_variation;
    });
    this.valuePercentage = false;
    this.variationPercentage = false;
    this.seconds = false;
  }

  setCategoryFlow(): void {
    this.metricsService.getMetrics().subscribe((data) => {
      var metrics = data[this.selectedPeriod].categories[this.selectedCategory].metrics
      this.value = metrics.exposure_perc;
      this.variation = metrics.exposure_perc_variation;
    });
    this.valuePercentage = true;
    this.variationPercentage = true;
    this.seconds = false;
  }

  setEngagedShoppers(): void {
    this.metricsService.getMetrics().subscribe((data) => {
      var metrics = data[this.selectedPeriod].categories[this.selectedCategory].metrics;
      this.value = metrics.stopping_power_perc;
      this.variation = metrics.stopping_power_perc_variation;
    });
    this.valuePercentage = true;
    this.variationPercentage = true;
    this.seconds = false;
  }

  setAverageTime(): void {
    this.metricsService.getMetrics().subscribe((data) => {
      var metrics = data[this.selectedPeriod].categories[this.selectedCategory].metrics;
      this.value = metrics.engagement;
      this.variation = metrics.engagement_variation;
    });
    this.valuePercentage = false;
    this.variationPercentage = true;
    this.seconds = true;
  }

  setCategoryTotalConversion(): void {
    this.metricsService.getMetrics().subscribe((data) => {
      var metrics = data[this.selectedPeriod].categories[this.selectedCategory].metrics;
      this.value = metrics.conversion_perc;
      this.variation = metrics.conversion_perc_variation;
    });
    this.valuePercentage = true;
    this.variationPercentage = true;
    this.seconds = false;
  }

  setManufacturerConversion(): void {
    this.metricsService.getMetrics().subscribe((data) => {
      var metrics = data[this.selectedPeriod].categories[this.selectedCategory].manufacturers[this.selectedManufacturer].metrics;
      this.value = metrics.conversion_perc;
      this.variation = metrics.conversion_perc_variation;
    });
    this.valuePercentage = true;
    this.variationPercentage = true;
    this.seconds = false;
  }

  setCompetitorsConversion(): void {
    this.metricsService.getMetrics().subscribe((data) => {
      var metrics = data[this.selectedPeriod].categories[this.selectedCategory].manufacturers[this.selectedManufacturer].metrics;
      this.value = metrics.conversion_others_perc;
      this.variation = metrics.conversion_others_perc_variation;
    });
    this.valuePercentage = true;
    this.variationPercentage = true;
    this.seconds = false;  
  }

  setMissedOpportunities(): void {
    this.metricsService.getMetrics().subscribe((data) => {
      var metrics = data[this.selectedPeriod].categories[this.selectedCategory].manufacturers[this.selectedManufacturer].metrics;
      this.value = metrics.waste_perc;
      this.variation = metrics.waste_perc_variation;
    });
    this.valuePercentage = true;
    this.variationPercentage = true;
    this.seconds = false;  
  }

  ngOnChanges(changes: SimpleChanges) {
    this.setAllWidgets();
  }

  setAllWidgets(): void {
    switch (this.infoType) {
      case "storeFlow":
          this.setWidget(
            "assets/store-flow-icon.png",
            "Store flow",
            "Daily average of shoppers who entered the store in the period",
            "* variation on the absolute value"
          )
          this.setStoreFlow()
        break;

      case "categoryFlow":
          this.setWidget(
            "assets/category-flow-icon.png",
            "Category flow",
            "Percentage of shoppers who entered the store and visited the category",
            ""
          )
          this.setCategoryFlow();
        break;
      
      case "engagedShoppers":
          this.setWidget(
            "assets/engaged-shoppers-icon.png",
            "Engaged shoppers",
            "Percentage of shoppers who visited the category and engaged more than 10 seconds",
            ""
          )
          this.setEngagedShoppers();
        break;
      
      case "averageTime":
          this.setWidget(
            "assets/average-time-icon.png",
            "Average time",
            "Average time that shoppers spent in the category",
            "variation on the value of the product"
          )
          this.setAverageTime();
        break;
      
      case "categoryTotalConversion":
          this.setWidget(
            "assets/category-total-conversion-icon.png",
            "Total category conversion",
            "Percentage of engaged shoppers who purchased any product in the category",
            ""
          )
          this.setCategoryTotalConversion()
        break;
      
      case "manufacturerConversion":
          this.setWidget(
            "assets/manufacturer-conversion-icon.png",
            "Manufacturer conversion",
            "Percentage of engaged shoppers who bought from the manufacturer",
            ""
          )
          this.setManufacturerConversion()
        break;
      
      case "competitorsConversion":
          this.setWidget(
            "assets/competitors-conversion-icon.png",
            "Competitors conversion",
            "Percentage of engaged shoppers who bought from competitors",
            ""
          )
          this.setCompetitorsConversion()
        break;
      
      case "missedOpportunities":
          this.setWidget(
            "assets/missed-opportunities-icon.png",
            "Missed opportunities",
            "Percentage of engaged shoppers who didn't buy from the manufacturer",
            ""
          )
          this.setMissedOpportunities()
        break;
      
      default:
        // code...
        break;
    }
  }

  ngOnInit() {}
}
