import { Injectable } from '@angular/core';
import {Http, Response} from '@angular/http';
import 'rxjs/Rx';

@Injectable()
export class MetricsService {

  constructor(public http:Http) {}

  getMetrics() {
  	return this.http.get("assets/metrics.json")
  		.map((res:Response) => res.json().weeks);
  }
}
