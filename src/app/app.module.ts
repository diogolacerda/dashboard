import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';

import { AppComponent } from './app.component';
import { InfoWidgetComponent } from './info-widget/info-widget.component';
import { VelocityWidgetComponent } from './velocity-widget/velocity-widget.component';

import { MetricsService } from './metrics.service';
import { HorizontalChartComponent } from './horizontal-chart/horizontal-chart.component';


@NgModule({
  declarations: [
    AppComponent,
    InfoWidgetComponent,
    VelocityWidgetComponent,
    HorizontalChartComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [MetricsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
