import { Component, OnInit } from '@angular/core';
import { MetricsService } from './metrics.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Dashboard';
  
  categories = [];
  manufacturers = [];
  periods = [];

  selectedCategory: string;
  selectedManufacturer: string;
  selectedPeriod: string;

  constructor(private metricsService: MetricsService) { }

  isBrasilKirin(): boolean {
    console.log(this.selectedManufacturer == "ADRIA ALIM DO BRASIL LTDA");
    return this.selectedManufacturer == "ADRIA ALIM DO BRASIL LTDA"
  }

  getPeriods(): void {
    this.metricsService.getMetrics().subscribe((data) => {
      for (var week in data) {
        this.periods.push(week);
      }
      this.selectedPeriod = this.periods[0];
    });
  }

  getCategories(): void {
    this.categories = []
    var tempCategories = []
    this.metricsService.getMetrics().subscribe((data) => {
      for (var category in data[this.selectedPeriod].categories) {
        if (tempCategories.indexOf(category) == -1) {
          this.categories.push(category);
          tempCategories.push(category);
        }
      }
      if (this.categories.indexOf(this.selectedCategory) == -1) {
        this.selectedCategory = this.categories[0];
      }
    });
  }

  getManufacturers(): void {
    this.manufacturers = [];
    var tempManufacturers = [];
    this.metricsService.getMetrics().subscribe((data) => {
      for (var manufacturer in data[this.selectedPeriod].categories[this.selectedCategory].manufacturers) {
        if (tempManufacturers.indexOf(manufacturer) == -1) {
          this.manufacturers.push(manufacturer);
          tempManufacturers.push(manufacturer);
        }
      }
      if (this.manufacturers.indexOf(this.selectedManufacturer) == -1) {
        this.selectedManufacturer = this.manufacturers[0];
      }
    });
  }

  onPeriodChange(): void{
    this.getCategories()    
    this.getManufacturers()    
  }

  onCategoryChange(): void{
    this.getManufacturers()    
  }

  onManufacturerChange(): void {
    this.isBrasilKirin()
  }

  
  ngOnInit() {
    this.getPeriods()
    this.getCategories()
    this.getManufacturers()
    this.isBrasilKirin()
  }

  
}
