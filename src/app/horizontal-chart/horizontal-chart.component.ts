import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { MetricsService } from '../metrics.service';

@Component({
  selector: 'horizontal-chart',
  templateUrl: './horizontal-chart.component.html',
  styleUrls: ['./horizontal-chart.component.css']
})
export class HorizontalChartComponent implements OnInit, OnChanges {

  @Input() selectedPeriod: string;
  @Input() selectedCategory: string;
  @Input() selectedManufacturer: string;
  top_shares = []

  constructor(private metricsService: MetricsService) { }

  ngOnInit() {
  	this.setChart();
  }

  ngOnChanges() {
  	this.setChart();
  }

  setChart(): void {
  	this.metricsService.getMetrics().subscribe((data) => {
  		this.top_shares = data[this.selectedPeriod].categories[this.selectedCategory].metrics.top_share_brands
  	});
  }

}
