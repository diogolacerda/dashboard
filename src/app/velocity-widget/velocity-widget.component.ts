import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { MetricsService } from '../metrics.service';

@Component({
  selector: 'velocity-widget',
  templateUrl: './velocity-widget.component.html',
  styleUrls: ['./velocity-widget.component.css']
})
export class VelocityWidgetComponent implements OnInit, OnChanges {

  title = "Speed of purchase at category";
  text = "Accumulated time of shoppers exposed to the shelf to sell a unit at the category (in minutes)";
  value: number;
  variation: number;

  @Input() selectedPeriod: string;
  @Input() selectedCategory: string;

  constructor(private metricsService: MetricsService) { }

  setWidget(): void {
    this.metricsService.getMetrics().subscribe((data) => {
      var metrics = data[this.selectedPeriod].categories[this.selectedCategory].metrics;
      this.value = metrics.purchase_speed;
      this.value = this.value / 60;
      this.variation = metrics.purchase_speed_variation;
    });
    this.variation = 0;
  }

  ngOnInit() {
    this.setWidget()
  }

  ngOnChanges(changes: SimpleChanges) {
    this.setWidget()
  }

}
